package bgs.tmffjtl.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBgsSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBgsSecurityApplication.class, args);
	}
}
