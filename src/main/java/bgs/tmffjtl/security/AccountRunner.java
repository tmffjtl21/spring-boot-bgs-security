package bgs.tmffjtl.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import bgs.tmffjtl.security.account.Account;
import bgs.tmffjtl.security.account.AccountService;

@Component
public class AccountRunner implements ApplicationRunner{

	@Autowired
	AccountService accountService;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		Account keesun = accountService.createAccount("keesun", "1234");
		System.out.println(keesun.toString());
	}
	
}
